var express = require('express');
var app = express();


var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080

app.get('/',function(req,res){
	res.send('Hello World!');
});

app.listen(port,function(){
	console.log('Hello World App Listening');
});